import {initializeApp} from "firebase/app"
import {getFirestore} from "firebase/firestore"

const firebaseConfig = {
  apiKey: "AIzaSyDYZc-7yRG0PmOH-aXHxi2UGLN_AeOdjNg",
  authDomain: "covid-exposure-d7e2e.firebaseapp.com",
  projectId: "covid-exposure-d7e2e",
  storageBucket: "covid-exposure-d7e2e.appspot.com",
  messagingSenderId: "922655856632",
  appId: "1:922655856632:web:e172d3057340d6c569a80d",
  measurementId: "G-QRH9CHFRPQ",
}

const app = initializeApp(firebaseConfig)
const db = getFirestore(app)

export {app, db}