import {GeoPoint, Timestamp} from "@firebase/firestore"
import {DateTime} from "luxon"

enum ContactType {
  CLOSE = "close",
  CASUAL = "casual",
  LOW_RISK = "low_risk",
}

function getTypeColor(contactType: ContactType): string {
  switch (contactType) {
    case ContactType.CLOSE:
      return "red"
    case ContactType.CASUAL:
      return "orange"
    case ContactType.LOW_RISK:
      return "yellow"
  }
}

class Site {
  id: any
  creation: DateTime
  start: DateTime
  end: DateTime
  location: GeoPoint
  address: string
  type: ContactType

  constructor(
    id: any,
    creation: Timestamp,
    start: Timestamp,
    end: Timestamp,
    location: GeoPoint,
    address: string,
    type: ContactType
  ) {
    this.id = id
    this.creation = DateTime.fromMillis(creation.toMillis())
    this.start = DateTime.fromMillis(start.toMillis())
    this.end = DateTime.fromMillis(end.toMillis())
    this.location = location
    this.address = address
    this.type = type
  }
}

export {ContactType, getTypeColor, Site}
