import {useMap} from "react-leaflet"

import {IconButton} from "@chakra-ui/react"
import {MdOutlineMyLocation} from "react-icons/md"

export default function Geolocate(props: any) {
  const map = useMap()

  return (
    <IconButton
      icon={<MdOutlineMyLocation />}
      onClick={() => map.locate({enableHighAccuracy: true, setView: true})}
      aria-label="Locate Me"
      bg="white"
      {...props}
    />
  )
}
