import {PropsWithChildren} from "react"

const POSITION_CLASSES = {
  bottomleft: "leaflet-bottom leaflet-left",
  bottomright: "leaflet-bottom leaflet-right",
  topleft: "leaflet-top leaflet-left",
  topright: "leaflet-top leaflet-right",
}

interface Props {
  position: keyof typeof POSITION_CLASSES
}

export default function Control({position, children, ...props}: PropsWithChildren<Props>) {
  return (
    <div className={POSITION_CLASSES[position]}>
      <div className="leaflet-control" {...props}>{children}</div>
    </div>
  )
}

export type {Props as ControlProps}
