import {useMap} from "react-leaflet"

import {IconButton, VStack, Divider} from "@chakra-ui/react"
import {MdAdd, MdRemove} from "react-icons/md"

export default function Zoom(props: any) {
  const map = useMap()
  map.removeControl(map.zoomControl)

  return (
    <VStack spacing={0} divider={<Divider borderWidth={1}/>} {...props}>
      <IconButton
        icon={<MdAdd />}
        onClick={() => map.zoomIn()}
        aria-label="Zoom In"
        borderBottomRadius={0}
        bg="white"
      />
      <IconButton
        icon={<MdRemove />}
        onClick={() => map.zoomOut()}
        aria-label="Zoom Out"
        borderTopRadius={0}
        bg="white"
      />
    </VStack>
  )
}
