import {useState} from "react"

import {Flex, FormControl, InputGroup, InputLeftElement} from "@chakra-ui/react"
import {
  AutoComplete,
  AutoCompleteInput,
  AutoCompleteItem,
  AutoCompleteList,
} from "@choc-ui/chakra-autocomplete"

import {MdSearch} from "react-icons/md"

export default function Search() {
  const [input, setInput] = useState<string>()
  const [predictions, setPredictions] = useState<string[]>()

  const handleChange = async (event: any) => {
    setInput(event.target.value)

    const data = await (await fetch(`/api/autocomplete?input=${input}`)).json()
    setPredictions(data)
  }

  return (
    <Flex justify="center" align="center" w="full">
      <FormControl>
        <AutoComplete openOnFocus closeOnSelect freeSolo>
          <InputGroup>
            <AutoCompleteInput
              bg="white"
              placeholder="Search..."
              value={input}
              onChange={async (event) => handleChange(event)}
            />
            <InputLeftElement children={<MdSearch />} />
          </InputGroup>
          <AutoCompleteList>
            {predictions?.map((prediction) => (
              <AutoCompleteItem key={prediction} value={prediction} />
            ))}
          </AutoCompleteList>
        </AutoComplete>
      </FormControl>
    </Flex>
  )
}
