import {MapContainer, MapContainerProps, TileLayer} from "react-leaflet"

import SiteMarker from "@/components/marker/SiteMarker"

import Control from "@/components/controls/Control"
import Zoom from "@/components/controls/Zoom"
import Geolocate from "@/components/controls/Geolocate"
import Search from "@/components/controls/Search"

import {chakra, ChakraProps, VStack} from "@chakra-ui/react"
const ChakraMapContainer = chakra(MapContainer)

import {Site} from "@/utils/types"
interface Props extends ChakraProps, MapContainerProps {
  sites: Site[]
}

export default function Map({sites, ...props}: Props) {
  return (
    <>
      <ChakraMapContainer center={[0, 0]} zoom={2} {...props}>
        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />

        {sites.map((site) => (
          <SiteMarker key={site.id} site={site} />
        ))}


        <Control position="topleft">
          <Search />
        </Control>
        <Control position="bottomleft">
          <VStack spacing={2} alignItems="flex-start">
            <Zoom />
            <Geolocate />
          </VStack>
        </Control>
      </ChakraMapContainer>
    </>
  )
}
