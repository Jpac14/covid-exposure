import {FC} from "react"

import {Badge, VStack, HStack, Icon, Text, Heading, ChakraProps} from "@chakra-ui/react"

import {Site, getTypeColor} from "@/utils/types"

import {IconType} from "react-icons/lib"
import {MdToday, MdTimelapse, MdAdd} from "react-icons/md"

interface Props extends ChakraProps, Site {}

const IconText: FC<{icon: IconType}> = ({icon, children, ...props}) => (
  <HStack spacing={1} {...props}>
    <Icon as={icon} boxSize={5} />
    <Text>{children}</Text>
  </HStack>
)

export default function SiteCard({creation, start, end, location, address, type, ...props}: Props) {
  return (
    <VStack alignItems="flex-start" {...props}>
      <Heading fontSize="sm">{address}</Heading>
      <Badge rounded="full" px={2} w="fit-content" colorScheme={getTypeColor(type)}>
        {type}
      </Badge>

      <IconText icon={MdToday}>Date: {start.toFormat("dd/MM/yyyy")}</IconText>
      <IconText icon={MdTimelapse}>
        Time: {start.toFormat("HH:mm")} - {end.toFormat("HH:mm")}
      </IconText>
      <IconText icon={MdAdd}>Created at: {creation.toFormat("dd/MM/yyyy HH:mm")}</IconText>
    </VStack>
  )
}
