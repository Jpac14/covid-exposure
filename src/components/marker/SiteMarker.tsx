import {Marker, Popup, useMapEvent} from "react-leaflet"

import {Site} from "@/utils/types"

import SiteCard from "./SiteCard"
import getTypeIcon from "./Icon"

interface Props {
  site: Site
}

export default function SiteMarker({site, ...props}: Props) {
  const map = useMapEvent("popupopen", () => {
    map.flyTo([site.location.latitude, site.location.longitude], 15)
  })
  
  return (
    <Marker
      position={[site.location.latitude, site.location.longitude]}
      icon={getTypeIcon(site.type)}
      {...props}
    >
      <Popup>
        <SiteCard {...site} />
      </Popup>
    </Marker>
  )
}