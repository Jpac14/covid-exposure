import {divIcon, DivIcon} from "leaflet"
import {ContactType} from "@/utils/types"

const createIcon = (className: String) => {
  return divIcon({
    className: `icon ${className}`,
  })
}

const CloseIcon = createIcon("close-icon")
const CasualIcon = createIcon("casual-icon")
const LowRiskIcon = createIcon("lowrisk-icon")

export default function getTypeIcon(type: ContactType): DivIcon {
  switch (type) {
    case ContactType.CLOSE:
      return CloseIcon
    case ContactType.CASUAL:
      return CasualIcon
    case ContactType.LOW_RISK:
      return LowRiskIcon
  }
}