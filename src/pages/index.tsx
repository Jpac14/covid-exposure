import dynamic from "next/dynamic"

import {db} from "@/utils/firebase"
import {useCollectionOnce} from "react-firebase-hooks/firestore"
import {collection} from "@firebase/firestore"

import {Site} from "@/utils/types"

const Map = dynamic(() => import("../components/Map"), {
  ssr: false,
})

export default function Home() {
  const [snapshot, loading, error] = useCollectionOnce(collection(db, "exposure_sites"))

  if (loading) return <>Loading...</>
  if (error) return <>Error: {error.message}</>
  if (!snapshot) return <>No data</>

  const sites: Site[] = snapshot.docs.map((doc) => {
    const data = doc.data()
    return new Site(
      data.id,
      data.creation,
      data.start,
      data.end,
      data.location,
      data.address,
      data.type
    )
  })

  return (
    <>
      <Map w="100vw" h="100vh" sites={sites} />
    </>
  )
}
