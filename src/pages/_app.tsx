import {ChakraProvider, extendTheme} from "@chakra-ui/react"
import type {AppProps} from "next/app"

import "@fontsource/inter"
import "../styles/global.css"

import "@/utils/firebase"

const theme = extendTheme({
  fonts: {
    body: "Inter",
    heading: "Inter"
  }
})

export default function App({Component, pageProps}: AppProps) {
  return (
    <ChakraProvider theme={theme}>
      <Component {...pageProps} />
    </ChakraProvider>
  )
}

