import { NextApiRequest, NextApiResponse } from "next";

import {Client, LatLng} from "@googlemaps/google-maps-services-js"
const client = new Client({})

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  const {input, latlng} = req.query as {input: string, latlng: LatLng}

  const data = await client.placeAutocomplete({
    params:  {
      input: input,
      location: latlng || [0, 0],
      key: process.env.GOOGLE_MAPS_API_KEY || "",
    }
  })

  const predictions = data.data.predictions.map((prediction) => prediction.description)
  return res.status(200).json(predictions)
}